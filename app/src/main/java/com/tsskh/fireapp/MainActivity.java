package com.tsskh.fireapp;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.tsskh.fireapp.adapter.PointOfViewAdapter;
import com.tsskh.fireapp.model.ViewPoint;

import java.security.MessageDigest;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements PointOfViewAdapter.RecyclerViewItemOnClickListener {

    private DatabaseReference mDatabase;

    private Button btnLink,btnPiture,btnVideo;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;


    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private PointOfViewAdapter pointOfViewAdapter;
    ArrayList<ViewPoint> viewPointsList = new ArrayList<>();
    // data to populate the RecyclerView with

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("pointsofview");
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<ArrayList<ViewPoint>> t = new GenericTypeIndicator<ArrayList<ViewPoint>>() {};
                mLayoutManager = new LinearLayoutManager(MainActivity.this);
                mRecyclerView.setLayoutManager(mLayoutManager);
                pointOfViewAdapter= new PointOfViewAdapter(MainActivity.this, dataSnapshot.getValue(t));
                pointOfViewAdapter.setOnItemClickListener(MainActivity.this);
                mRecyclerView.setAdapter(pointOfViewAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("MainActivity", databaseError.toException());

            }
        };
        mDatabase.addValueEventListener(postListener);


       /* btnLink = (Button)findViewById(R.id.btn_share_link);
        btnPiture = (Button)findViewById(R.id.btn_share_picture);
        btnVideo = (Button)findViewById(R.id.btn_share_video);


        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        btnLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setQuote("(Testing)7 Principles to live a Good Life")
                        .setContentUrl(Uri.parse("https://www.youtube.com/watch?v=XvRMl7jz1O8"))
                        .build();
                if(shareDialog.canShow(ShareLinkContent.class)){
                    shareDialog.show(linkContent);
                }
            }
        });*/
    }

   /* public void OtherCode(){
         mDatabase = FirebaseDatabase.getInstance().getReference().child("viewpoints");
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<ArrayList<ViewPoint>> t = new GenericTypeIndicator<ArrayList<ViewPoint>>() {};
                ArrayList<ViewPoint> viewPointsList = dataSnapshot.getValue(t);
                for(int i=0; i< viewPointsList.size();i++) {
                    Log.w("MainActivity", "Title : "+viewPointsList.get(i).getTitle());
                    Log.w("MainActivity", "Title : "+viewPointsList.get(i).getDescription());
                    Log.w("MainActivity", "Title : "+viewPointsList.get(i).getAuthor());
                    Log.w("MainActivity", "Title : "+viewPointsList.get(i).getExclude());
                    Log.w("MainActivity", "___________________________________________");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("MainActivity", databaseError.toException());

            }
        };
        mDatabase.addValueEventListener(postListener);
    }*/

    private void printKeyHash(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.tsskh.fireapp",
                    PackageManager.GET_SIGNATURES);
                    for (Signature signature : info.signatures){
                        MessageDigest md = MessageDigest.getInstance("SHA");
                        md.update(signature.toByteArray());
                        Log.d("keyHash", Base64.encodeToString(md.digest(),Base64.DEFAULT));
                    }

        }catch (Exception e){

        }
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}
