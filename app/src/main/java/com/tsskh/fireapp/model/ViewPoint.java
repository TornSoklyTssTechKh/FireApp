package com.tsskh.fireapp.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by User on 1/31/2018.
 */
@IgnoreExtraProperties
public class ViewPoint {
    public String title;
    public String writer;
    public String exclude;
    public String author;

    public ViewPoint(){}
    public ViewPoint(String title, String writer, String exclude, String author) {
        this.title = title;
        this.writer = writer;
        this.exclude = exclude;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getwriter() {
        return writer;
    }

    public void setwriter(String writer) {
        this.writer = writer;
    }

    public String getExclude() {
        return exclude;
    }

    public void setExclude(String exclude) {
        this.exclude = exclude;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
