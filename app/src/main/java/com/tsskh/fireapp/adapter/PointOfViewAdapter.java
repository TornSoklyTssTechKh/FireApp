package com.tsskh.fireapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tsskh.fireapp.R;
import com.tsskh.fireapp.model.ViewPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 2/15/2018.
 */

public class PointOfViewAdapter extends RecyclerView.Adapter<PointOfViewAdapter.MyViewHolder>{


    private Context mContext;
    private ArrayList<ViewPoint> mData;
    private LayoutInflater mInflater;
    private RecyclerViewItemOnClickListener mClickListener;

    // data is passed into the constructor
    public PointOfViewAdapter(Context context, ArrayList<ViewPoint> viewPointList) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = viewPointList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       // holder.txtBook.setText(mData.get(position).getExclude().toString());
       // holder.txtAuthor.setText(mData.get(position).getAuthor().toString());
        holder.txtContent.setText(mData.get(position).getTitle().toString()+"\n("+
                                    mData.get(position).getwriter().toString()+")");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
    public void setOnItemClickListener(RecyclerViewItemOnClickListener onItemClickListener) {
        this.mClickListener = onItemClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtBook,txtAuthor,txtContent;

        MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            //txtBook = (TextView) itemView.findViewById(R.id.txt_book);
          //  txtAuthor = (TextView) itemView.findViewById(R.id.txt_author);
            txtContent = (TextView) itemView.findViewById(R.id.txt_content);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view,getPosition());
            }
        }
    }

    public interface RecyclerViewItemOnClickListener {
        void onItemClick(View view, int position);
    }
}
